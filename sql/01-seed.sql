INSERT INTO "users" (
    id, uuid, username, password, created_at, updated_at
) VALUES
    (1, uuid_generate_v4(), 'admin', crypt('admin', gen_salt('bf', 8)), now(), now()),
    (2, uuid_generate_v4(), 'guest', crypt('guest', gen_salt('bf', 8)), now(), now()),
    (3, uuid_generate_v4(), 'user-manager', crypt('user-manager', gen_salt('bf', 8)), now(), now()),
    (4, uuid_generate_v4(), 'insurance-manager', crypt('insurance-manager', gen_salt('bf', 8)), now(), now()),
    (5, uuid_generate_v4(), 'broker', crypt('broker', gen_salt('bf', 8)), now(), now()),
    (6, uuid_generate_v4(), 'manager', crypt('manager', gen_salt('bf', 8)), now(), now());

INSERT INTO roles (
    id, name, created_at, updated_at
) VALUES
    (1, 'admin', now(), now()),
    (2, 'guest', now(), now()),
    (3, 'use-manager', now(), now()),
    (4, 'insurance-manager', now(), now()),
    (5, 'broker', now(), now());

INSERT INTO permissions (
    id, action, created_at, updated_at
) VALUES
    (1, '*',  now(), now()),
    (2, 'user:create',  now(), now()),
    (3, 'user:read',    now(), now()),
    (4, 'user:update',  now(), now()),
    (5, 'user:delete',  now(), now()),
    (6, 'insurance:create', now(), now()),
    (7, 'insurance:read',   now(), now()),
    (8, 'insurance:update', now(), now()),
    (9, 'insurance:delete', now(), now());

INSERT INTO user_roles (
    user_id, role_id
) VALUES
    (1, 1), -- admin
    (2, 2), -- guest
    (3, 3), -- user-manager
    (4, 4), -- insurance-manager
    (5, 5), -- broker

    (6, 3), -- manager | user-manager
    (6, 4); -- manager | insurance-manager

INSERT INTO role_permissions (
    role_id, permission_id
) VALUES
    (1, 1), -- admin    - *

    (2, 3), -- guest    - user:read

    (3, 2), -- user-manager  - user:create
    (3, 3), -- user-manager  - user:update
    (3, 4), -- user-manager  - user:read

    (4, 6), -- insurance-manager  - insurance:create
    (4, 7), -- insurance-manager  - insurance:read
    (4, 8), -- insurance-manager  - insurance:update

    (5, 2), -- broker   - user:read,
    (5, 7); -- broker   - insurance:read,
