CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS "pgcrypto";

CREATE TABLE IF NOT EXISTS "users" (
	id BIGSERIAL NOT NULL PRIMARY KEY,
	uuid uuid NOT NULL,
	username VARCHAR(30) NOT NULL,
	password VARCHAR(100) NOT NULL,
	created_at TIMESTAMP NOT NULL,
	updated_at TIMESTAMP,
	CONSTRAINT users_ukey UNIQUE (username)
);

CREATE TABLE IF NOT EXISTS "roles" (
	id BIGSERIAL NOT NULL PRIMARY KEY,
	name VARCHAR(20) NOT NULL UNIQUE,
	created_at TIMESTAMP NOT NULL,
	updated_at TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "permissions" (
	id BIGSERIAL NOT NULL PRIMARY KEY,
	action VARCHAR(40) NOT NULL UNIQUE,
	created_at TIMESTAMP NOT NULL,
	updated_at TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "user_roles" (
	user_id BIGINT NOT NULL,
	role_id BIGINT NOT NULL,
	CONSTRAINT user_role_pkey PRIMARY KEY (user_id, role_id)
);

CREATE TABLE IF NOT EXISTS "role_permissions" (
	role_id BIGINT NOT NULL,
	permission_id BIGINT NOT NULL,
	CONSTRAINT role_permission_pkey PRIMARY KEY (role_id, permission_id)
);
