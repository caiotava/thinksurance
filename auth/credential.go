package auth

import (
	"errors"
	"fmt"
	"strings"

	"golang.org/x/crypto/bcrypt"
)

const (
	minPasswordLen = 5
	maxPasswordLen = 30
	minUsernameLen = 3
	maxUsernameLen = 30

	lenghtErrorMsg = "%s: must be between %d and %d characters"
)

type credential struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func (c credential) validateHash(hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(c.Password))

	return err == nil
}

func (c credential) validate() error {
	var errorMsgs []string

	usernameLen := len(c.Username)

	if usernameLen < minUsernameLen || usernameLen > maxUsernameLen {
		msg := fmt.Sprintf(lenghtErrorMsg, "username", minUsernameLen, maxUsernameLen)
		errorMsgs = append(errorMsgs, msg)
	}

	passwordLen := len(c.Password)

	if passwordLen < minPasswordLen || passwordLen > maxPasswordLen {
		msg := fmt.Sprintf(lenghtErrorMsg, "password", minPasswordLen, maxPasswordLen)
		errorMsgs = append(errorMsgs, msg)
	}

	if len(errorMsgs) > 0 {
		return errors.New(strings.Join(errorMsgs, "; "))
	}

	return nil
}
