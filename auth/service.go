package auth

import (
	"bitbucket.org/caiotava/thinksurance/user"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

// Service represents the AuthenticationService, it provide the resources
// for the authentication flow.
type Service struct {
	DB *gorm.DB
}

// UserByCredentials returns the user related to the credentials passed by parameter
func (r Service) UserByCredentials(cred credential) (user.User, error) {
	var userDB user.User

	result := r.DB.Preload("Roles.Permissions").Preload(clause.Associations).
		First(&userDB, "username = ?", cred.Username)

	if result.Error != nil {
		return user.User{}, result.Error
	}

	if !cred.validateHash(userDB.Password) {
		return user.User{}, gorm.ErrRecordNotFound
	}

	return userDB, nil
}
