package auth

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gorm.io/gorm"
)

// Handler manages the HTTP request for the auth resource.
type Handler struct {
	Service Service
}

// ServeHttp handles HTTP requests for the auth resource.
func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		h.errorResponse(w, "", http.StatusMethodNotAllowed)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	var cred credential

	if err := json.NewDecoder(r.Body).Decode(&cred); err != nil {
		h.errorResponse(w, fmt.Sprintf("invalid json: %s", err), http.StatusBadRequest)
		return
	}

	if err := cred.validate(); err != nil {
		h.errorResponse(w, err.Error(), http.StatusBadRequest)
		return
	}

	u, err := h.Service.UserByCredentials(cred)

	if err == gorm.ErrRecordNotFound {
		h.errorResponse(w, "", http.StatusUnauthorized)
		return
	}

	if err != nil {
		h.errorResponse(w, fmt.Sprintf("error getting user by credentials: %s", err), http.StatusInternalServerError)
		return
	}

	if err = json.NewEncoder(w).Encode(u); err != nil {
		h.errorResponse(w, fmt.Sprintf("error encoding user to json: %s", err), http.StatusInternalServerError)
		return
	}
}

func (h *Handler) errorResponse(w http.ResponseWriter, msg string, code int) {
	if msg != "" && code >= http.StatusInternalServerError {
		log.Print(msg)
		msg = http.StatusText(code)
	}

	if msg == "" {
		msg = http.StatusText(code)
	}

	w.WriteHeader(code)
	errorMsg := map[string]string{"error": msg}

	if err := json.NewEncoder(w).Encode(errorMsg); err != nil {
		log.Printf("error handling error response: %s", err)
	}
}
