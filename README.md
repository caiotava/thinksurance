# Thinksurance

This project implements the solution for three distinct tasks

1. **JSON parsing basic skills:** Read the https://exchangeratesapi.io/ API and list all data in any output format
2. **Algorithm:** PigLatin's word validator.
3. **Small project:** Login application with users, roles, and permissions.

## How to Run

### Dependencies
To run the project locally you must have:

* [GoLang 1.15](https://golang.org/)
* [docker](https://www.docker.com/)
* [docker-compose](https://docs.docker.com/compose/) 

## Test

Just run:

```bash
make test
```

## Running the projects
- **Run Exchange Rates Client**
```bash
# Running exchange-rates directly with golang.
$ make run-exchange-rate

# Using docker to run exchange-rates, It's necessary to build the image before:
$ make docker-build
$ make run-exchange-rate-docker
```

- **Run API** 
```bash
$ make run-api-docker
```

## Authenticating user

After running the login-api, it'll be accessible on [http://localhost:8080]

- **Authenticate user with credentials**
```bash
$ curl -XPOST 'localhost:8080/auth' -H'Content-Type: application/json' --data-raw '{"username": "guest", "password": "guest"}'
```

#### List of available users

| Username          | Password           |
| ----------------- | ------------------ |
| admin             | admin              |
| guest             | guest              |
| user-manager      | user-manager       |
| insurance-manager | insurance-manager  |
| broker            | broker             |
| manager           | manager            |

