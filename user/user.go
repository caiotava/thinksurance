package user

import (
	"time"
)

// User represents the related data of the API users.
type User struct {
	ID        uint      `json:"-" gorm:"primaryKey"`
	UUID      string    `json:"uuid" gorm:"default:uuid_generate_v4()"`
	Username  string    `json:"username"`
	Password  string    `json:"-"`
	Roles     []Role    `json:"roles" gorm:"many2many:user_roles"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
