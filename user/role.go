package user

import "time"

// Role represents the user's roles for handling the access control.
type Role struct {
	ID          uint         `json:"-" gorm:"primaryKey"`
	Name        string       `json:"name"`
	Permissions []Permission `json:"permissions" gorm:"many2many:role_permissions"`
	CreatedAt   time.Time    `json:"-"`
	UpdatedAt   time.Time    `json:"-"`
}

// Permission represents all actions that roles can perform.
type Permission struct {
	ID        uint      `json:"-" gorm:"primaryKey"`
	Action    string    `json:"action"`
	CreatedAt time.Time `json:"-"`
	UpdatedAt time.Time `json:"-"`
}
