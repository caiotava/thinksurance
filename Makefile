GO_PACKAGES ?= `go list ./...`
GO_TEST_FLAGS =
GO_LDFLAGS = -s -w

all: test build

build: build-api build-exchange-rate

build-api:
	go build -ldflags="${GO_LDFLAGS}" -o login-api cmd/loginapi/main.go

build-exchange-rate:
	go build -ldflags="${GO_LDFLAGS}" -o exchange-rate cmd/exchangerate/main.go

docker-build:
	docker build -t caiotava/thinksurance .

run-api:
	go run cmd/loginapi/main.go

run-api-docker:
	docker-compose up -d

down-api-docker:
	docker-compose down

run-exchange-rate:
	go run cmd/exchangerate/main.go

run-exchange-rate-docker:
	docker run -it --net=host --entrypoint usr/bin/exchange-rate caiotava/thinksurance

test:
	go test ${GO_TEST_FLAGS} ${GO_PACKAGES}

.PHONY:
	all build build-api build-exchange-rate docker-build run-api run-api-docker down-api-docker run-exchange-rate run-exchange-rate-docker test
