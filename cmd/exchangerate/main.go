package main

import (
	"encoding/json"
	"log"
	"os"

	"bitbucket.org/caiotava/thinksurance/exchangerate"
)

func main() {
	exchangeRates, err := exchangerate.ExchangeRates()

	if err != nil {
		log.Fatalf("error getting exchange rates: %s", err)
	}

	encoder := json.NewEncoder(os.Stdout)
	encoder.SetIndent("", "  ")

	if err = encoder.Encode(exchangeRates); err != nil {
		log.Fatalf("error encoding exchange rates: %s", err)
	}
}
