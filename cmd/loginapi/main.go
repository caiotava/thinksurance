package main

import (
	"log"
	"net/http"

	"bitbucket.org/caiotava/thinksurance/auth"
	"bitbucket.org/caiotava/thinksurance/config"
	"bitbucket.org/caiotava/thinksurance/db"
)

func main() {
	db, err := db.SetupDatabase()

	if err != nil {
		log.Fatalf("error connecting database: %s", err)
	}

	service := auth.Service{DB: db}

	http.Handle("/auth", &auth.Handler{Service: service})

	serverPort := config.Value("API_PORT")

	log.Printf("starting HTTP server on port: %q", serverPort)

	log.Fatal(http.ListenAndServe(":"+serverPort, nil))
}
