package config

import "os"

var (
	defaultConfig = map[string]string{
		"API_PORT":    "8080",
		"DB_HOST":     "localhost",
		"DB_USER":     "loginapi",
		"DB_NAME":     "loginapi",
		"DB_PASSWORD": "loginapi",
		"DB_PORT":     "5432",

		"EXCHANGE_API_HOST": "https://api.exchangeratesapi.io",
	}
)

// Value returns a configuration value.
// It tries to get the value from the environment variables.
// If the values aren't in environment variables, it returns
// the default value.
func Value(name string) string {
	if v := os.Getenv(name); v != "" {
		return v
	}

	return defaultConfig[name]
}
