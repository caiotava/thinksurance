package piglatin

import "testing"

func TestValidate(t *testing.T) {
	validateTests := []struct {
		word     string
		expected bool
	}{
		// No words
		{"", false},
		{"1234", false},
		{"$#$#$#", false},

		// English words
		{"Pig", false},
		{"stairway", false},
		{"Highway", false},
		{"latin", false},
		{"Latin", false},
		{"banana", false},
		{"thrusday", false},
		{"will", false},
		{"Enter", false},
		{"English", false},
		{"HTML", false},
		{"hyphenated", false},
		{"every", false},
		{"is", false},
		{"words", false},
		{"accompanying", false},
		{"JavaScript", false},

		// Words with Y in second character
		{"ymbolssay", false},
		{"yphenatedhway", false},

		// Words valid in both languages
		{"may", true},
		{"MAY", true},
		{"way", true},
		{"pay", true},
		{"say", true},
		{"day", true},
		{"everyday", true},

		// PigLatin's words
		{"igpay", true},
		{"igpway", true},
		{"ipigpway", true},
		{"Enterway", true},
		{"Enteray", true},
		{"ethay", true},
		{"Englishway", true},
		{"exttay", true},
		{"erehay", true},
		{"atthay", true},
		{"ouyay", true},
		{"antway", true},
		{"anslatedtray", true},
		{"intoway", true},
		{"Igpay", true},
		{"Atinlay", true},
		{"Isthay", true},
		{"isway", true},
		{"accomplishedway", true},
		{"iavay", true},
		{"isthay", true},
		{"andway", true},
		{"accompanyingway", true},
		{"AvaScriptjay", true},
		{"Otenay", true},
		{"atthay", true},
		{"enatedhyphay", true},
		{"ordsway", true},
		{"areway", true},
		{"eatedtray", true},
		{"asway", true},
		{"otway", true},
		{"ordsway", true},
		{"Ordsway", true},
		{"aymay", true},
		{"onsistcay", true},
		{"ofway", true},
		{"alphabeticway", true},
		{"aracterschay", true},
		{"onlyway", true},
		{"Away-ZAY", true},
		{"andway", true},
		{"away-zay", true},
		{"Allway", true},
		{"unctuationpay", true},
		{"umeralsnay", true},
		{"andway", true},
		{"itespacewhay", true},
		{"areway", true},
		{"otnay", true},
		{"odifiedmay", true},
		{"everyway", true},
		{"usdaythray", true},
		{"an'tcay", true},

		// pigLating's words without vowels
		{"HTMLAY", true},
		{"40thay", true},
	}

	for _, test := range validateTests {
		if Validate(test.word) != test.expected {
			t.Errorf("Validate(%q) = %v, want %v", test.word, !test.expected, test.expected)
		}
	}
}
