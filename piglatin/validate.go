package piglatin

import (
	"strings"
)

const suffix = "ay"
const vowels = "aeiou"

// Validate verifies if the word passed by argument is piglatin's word.
func Validate(word string) bool {
	word = strings.ToLower(word)
	wordNoSuffix := strings.TrimSuffix(word, suffix)

	if word == wordNoSuffix {
		return false
	}

	firstChar := word[0:1]

	return strings.Contains(vowels, firstChar) || !strings.ContainsAny(wordNoSuffix, vowels)
}
