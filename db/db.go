package db

import (
	"fmt"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"bitbucket.org/caiotava/thinksurance/config"
)

// SetupDatabase configures a new database connection with environment variables.
func SetupDatabase() (*gorm.DB, error) {
	dsn := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Europe/Berlin",
		config.Value("DB_HOST"),
		config.Value("DB_USER"),
		config.Value("DB_PASSWORD"),
		config.Value("DB_NAME"),
		config.Value("DB_PORT"),
	)

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		DisableForeignKeyConstraintWhenMigrating: true,
	})

	if err != nil {
		return nil, err
	}

	return db, nil
}
