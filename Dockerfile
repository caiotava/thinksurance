#build stage
FROM golang:1.15-alpine as builder

WORKDIR /thinksurance

COPY . .

RUN apk update && \
    apk add --no-cache ca-certificates make gcc musl-dev && update-ca-certificates

RUN make build

#deploy stage
FROM alpine:3.12

COPY --from=builder /thinksurance/login-api /usr/bin/login-api
COPY --from=builder /thinksurance/exchange-rate /usr/bin/exchange-rate

ENTRYPOINT ["/usr/bin/login-api"]
