package exchangerate

import (
	"encoding/json"
	"fmt"
	"time"
)

const dateFormat = "2006-01-02"

type exchangeDate time.Time

// ExchangeRate represents foreign exchange rates.
// This struct is based on the response of https://exchangeratesapi.io/
type ExchangeRate struct {
	Base  string             `json:"base"`
	Date  exchangeDate       `json:"date"`
	Rates map[string]float64 `json:"rates"`
}

// UnmarshalJSON customize the way to Unmarshalling JSON dates.
// This was necessary because the https://exchangeratesapi.io/
// response comes with a different date format than expected.
func (e *exchangeDate) UnmarshalJSON(b []byte) (err error) {
	var date time.Time
	var value = string(b)
	var format = fmt.Sprintf("%q", dateFormat)

	if date, err = time.Parse(format, value); err != nil {
		return err
	}

	*e = exchangeDate(date)

	return nil
}

// MarshalJSON customize the way to marshalling JSON dates.
// To keep the same date format as the Exchange Rates API.
func (e exchangeDate) MarshalJSON() ([]byte, error) {
	t := time.Time(e)

	return json.Marshal(t.Format(dateFormat))
}
