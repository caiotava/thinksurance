package exchangerate

import (
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/caiotava/thinksurance/config"
)

// ExchangeRates returns exchange rates based on the EUR currency.
func ExchangeRates() (ExchangeRate, error) {
	var exchangeRates ExchangeRate
	resp, err := http.Get(config.Value("EXCHANGE_API_HOST") + "/latest")

	if err != nil {
		return exchangeRates, fmt.Errorf("error requesting exchange rates api: %s", err)
	}

	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)

	if err = decoder.Decode(&exchangeRates); err != nil {
		return exchangeRates, fmt.Errorf("error decoding json: %s", err)
	}

	return exchangeRates, nil
}
